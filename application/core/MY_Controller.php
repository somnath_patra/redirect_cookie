<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct() {
        parent::__construct();

        if(empty($this->session->userdata('USER_SESSION')))
        {
			$cookie = array(
				'name'  => 'logged',
				'value'  => $_SERVER['REQUEST_URI'],
				'expire' => '86500',
			);

			set_cookie($cookie);
			redirect('Welcome');
        }
    }
}
